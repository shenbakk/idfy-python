"""
IDfy API client integration.
"""
import json
import requests
from constants import Url, Tasks
from errors import BadRequestError


class Client(Tasks):
    DEFAULTS = {
        'base_url_v2': Url.BASE_URL_V2,
        'base_url_v3': Url.BASE_URL_V3,
        'headers': {"content-Type": "application/json"},
        'tasks_config': Tasks._tasks_config,
    }

    def __init__(self, auth: str, **options):
        self.auth = auth
        self.DEFAULTS['headers'].update({'apikey': self.auth})
        self.base_url = self._set_base_url(**options)
        self.headers = self._validate_request_headers(**options)

    def _set_base_url(self, **options) -> str:
        """
        Set the api_endpoint based on client's initiation
        :param options: dict obj
        :return: api_endpoint url
        """
        api_endpoint = self.DEFAULTS['base_url_v2']
        if 'url' in options:
            api_endpoint = options['url']
        return api_endpoint

    @staticmethod
    def _update_api_body(task_type, task_id, data, group_id=None):
        """
        Update the api_body with
        :param task_type: task_type
        :param task_id: task_id
        :param data: part of request_body
        :param group_id: group_id
        :return: updated req_body
        """
        req_body = {
            'tasks': [
                {
                    'type': task_type,
                    'task_id': task_id,
                    'data': data
                }
            ]
        }

        if group_id:
            req_body['tasks'][0].update({'group_id': group_id})

        return req_body

    def _validate_request_headers(self, **options):
        if 'headers' in options:
            if isinstance(options['headers'], dict):
                if 'apikey' not in self.headers:
                    raise BadRequestError(
                        "No API-key provided. client_1 = Client('<YOUR_API_KEY>')")
                self.DEFAULTS.update(options['headers'])
            else:
                raise BadRequestError("Invalid headers provided. "
                                      "Correct format: {'content-Type': 'application/json'}")

        return self.DEFAULTS['headers']

    def _validate_request_data(self, data: dict, task_type: str, api_version: str):
        """
        validate request_data. Checks presence of mandate_fields
        :param data: part of request_body
        :param task_type: task_type
        :return: None
        """

        input_data_keys = data.keys()
        all_task_data_fields = []  # Array to append all fields

        task_schema = self.DEFAULTS['tasks_config'][api_version]['data_schema'][task_type]
        mandate_task_data_fields = task_schema['mandate_fields']

        if mandate_task_data_fields:  # Appending non-empty mandate_task_data_fields
            all_task_data_fields.append(*mandate_task_data_fields)

            # Checking if all mandate fields are included in the input_request_data
            for k in mandate_task_data_fields:
                if k not in input_data_keys:
                    raise BadRequestError(
                        "Insufficient data-fields provided. Expected mandatory fields "
                        "- {0}".format(", ".join(mandate_task_data_fields)))

        optional_task_data_fields = task_schema['optional_fields']
        if optional_task_data_fields:
            all_task_data_fields.append(*optional_task_data_fields)

        any_task_data_fields = task_schema['any']
        if any_task_data_fields:
            all_task_data_fields.append(*any_task_data_fields)

            any_fields_flag = None  # Flagging presence of any field from any_task_data_fields in input_data_keys
            for k in any_task_data_fields:
                if k in input_data_keys:
                    any_fields_flag = True
                    break

            if not any_fields_flag:  # Raising error if any_fields_flag is None
                raise BadRequestError(
                    "Insufficient data-fields provided. Expected any of these fields"
                    " - {0}".format(", ".join(any_task_data_fields)))

        # Checking if any of the input data_fields are not required
        for k in input_data_keys:
            if k not in all_task_data_fields:
                raise BadRequestError("Unexpected data-field provided in data - \"{0}\" in {1}. \n"
                                      "Acceptable fields - {2}.".format(k, data, ", ".join(all_task_data_fields)))

    def _validate_request_arguments(self, task_type: str, task_id: str,
                                    data: dict, api_version, group_id=None):
        """
        Validate json_body part of the api_request
        :param task_type: task_type
        :param task_id: task_id
        :param data: part of request_body
        :param api_version: v2/v3
        :param group_id: group_id
        :return: req_body
        """
        if not isinstance(data, dict):
            raise BadRequestError("Invalid data format. Expected format is dictionary, eg: {'doc_url': '<URL>'}")

        if not isinstance(task_id, str):
            raise BadRequestError("Invalid task_id format. Expected format is string, eg: '<TASK_ID>'")

        if not isinstance(task_type, str):
            raise BadRequestError("Invalid task_type format. Expected format is string, eg: '<TASK_TYPE>'")
        elif task_type not in self.DEFAULTS['tasks_config'][api_version]['available_tasks']:
            raise BadRequestError(
                "Invalid task_type requested. Refer the doc for task_types - "
                "https://api-docs.idfy.com/v2/#task-types")

        if not isinstance(group_id, str) if group_id else False:
            raise BadRequestError("Invalid group_id format. Expected format is string, eg: '<GROUP_ID>'")

        self._validate_request_data(data=data, task_type=task_type, api_version=api_version)

        req_body = self._update_api_body(task_type=task_type,
                                         task_id=task_id, data=data, group_id=group_id)

        return req_body

    def post(self, task_type: str, task_id: str, data: dict, group_id=None):
        """
        post_request to EVE-API
        :param task_type: task_type
        :param task_id: task_id
        :param data: part of the request_body
        :param group_id: group_id
        :return: API_request's request_id and status
        """
        req_body = self._validate_request_arguments(task_type=task_type,
                                                    task_id=task_id, data=data, api_version="v2", group_id=group_id)

        post_req = requests.post(url=self.DEFAULTS['base_url_v2'], data=json.dumps(req_body),
                                 headers=self.headers)

        return json.loads(post_req.text)

    def get(self, request_id: str):
        """
        get request to retrieve response from EVE-API with request_id
        :param request_id: request_id
        :return: API_call's response
        """
        if not isinstance(request_id, str):
            raise BadRequestError("Invalid request_id format. Expected format is string.")

        params = {'request_id': request_id}
        get_res = requests.get(url=self.DEFAULTS['base_url_v2'],
                               headers=self.headers, params=params)

        return json.loads(get_res.text)
